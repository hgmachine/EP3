***************************************************************

Milestones >> EP3

HGM - 07/12/2016

***************************************************************

1a Etapa: realizada

Preparação de uma máquina virtual para hospedagem de uma distribuição
Linux (Ubuntu) e instalação dos pacotes para Ruby on Rails

2a Etapa: realizada

Estudo estruturado da linguagem Ruby e de recursos para geraçãõ de
aplicações. Ambiente Rails, gems, geradores, etc.

3a Etapa: atual

Início da codificação do projeto. Desenvolvimento de uma aplicação
simples para cadastro de usuários, permitindo aos mesmos a visualização
de seus dados, criação de dados, e visualização de informações. O sistema
deverá, preferencialmente, ter um login com senha e permissões diferenciadas
de acesso às informações gerenciadas pela aplicação em questão.

