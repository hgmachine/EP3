class CreateAtributes < ActiveRecord::Migration
  def change
    create_table :atributes do |t|
      t.integer :Forca
      t.integer :Reflexo
      t.integer :Saude
      t.integer :Intuicao
      t.integer :Vontade
      t.integer :Presenca
      t.references :player, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
