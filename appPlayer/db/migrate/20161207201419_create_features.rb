class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.text :equipamentos
      t.text :proficiencias
      t.references :player, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
