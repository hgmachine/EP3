class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :especie
      t.string :altura
      t.string :peso
      t.string :gravidade
      t.string :sexo
      t.string :idade

      t.timestamps null: false
    end
  end
end
