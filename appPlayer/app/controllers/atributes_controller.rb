class AtributesController < ApplicationController
  before_action :set_atribute, only: [:show, :edit, :update, :destroy]

  # GET /atributes
  # GET /atributes.json
  def index
    @atributes = Atribute.all
  end

  # GET /atributes/1
  # GET /atributes/1.json
  def show
  end

  # GET /atributes/new
  def new
    @atribute = Atribute.new
  end

  # GET /atributes/1/edit
  def edit
  end

  # POST /atributes
  # POST /atributes.json
  def create
    @atribute = Atribute.new(atribute_params)

    respond_to do |format|
      if @atribute.save
        format.html { redirect_to @atribute, notice: 'Atribute was successfully created.' }
        format.json { render :show, status: :created, location: @atribute }
      else
        format.html { render :new }
        format.json { render json: @atribute.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /atributes/1
  # PATCH/PUT /atributes/1.json
  def update
    respond_to do |format|
      if @atribute.update(atribute_params)
        format.html { redirect_to @atribute, notice: 'Atribute was successfully updated.' }
        format.json { render :show, status: :ok, location: @atribute }
      else
        format.html { render :edit }
        format.json { render json: @atribute.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /atributes/1
  # DELETE /atributes/1.json
  def destroy
    @atribute.destroy
    respond_to do |format|
      format.html { redirect_to atributes_url, notice: 'Atribute was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_atribute
      @atribute = Atribute.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def atribute_params
      params.require(:atribute).permit(:Forca, :Reflexo, :Saude, :Intuicao, :Vontade, :Presenca, :player_id)
    end
end
