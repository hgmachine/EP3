class Player < ActiveRecord::Base
    
    has_one :atribute
    has_one :feature

    accepts_nested_attributes_for :atribute
    accepts_nested_attributes_for :feature
    
    def self.search(query)
	where("name like ?","%#{query}%")
    end
end
