json.extract! feature, :id, :equipamentos, :proficiencias, :player_id, :created_at, :updated_at
json.url feature_url(feature, format: :json)