json.extract! player, :id, :name, :especie, :altura, :peso, :gravidade, :sexo, :idade, :created_at, :updated_at
json.url player_url(player, format: :json)