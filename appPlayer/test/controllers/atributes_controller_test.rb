require 'test_helper'

class AtributesControllerTest < ActionController::TestCase
  setup do
    @atribute = atributes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:atributes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create atribute" do
    assert_difference('Atribute.count') do
      post :create, atribute: { Forca: @atribute.Forca, Intuicao: @atribute.Intuicao, Presenca: @atribute.Presenca, Reflexo: @atribute.Reflexo, Saude: @atribute.Saude, Vontade: @atribute.Vontade, player_id: @atribute.player_id }
    end

    assert_redirected_to atribute_path(assigns(:atribute))
  end

  test "should show atribute" do
    get :show, id: @atribute
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @atribute
    assert_response :success
  end

  test "should update atribute" do
    patch :update, id: @atribute, atribute: { Forca: @atribute.Forca, Intuicao: @atribute.Intuicao, Presenca: @atribute.Presenca, Reflexo: @atribute.Reflexo, Saude: @atribute.Saude, Vontade: @atribute.Vontade, player_id: @atribute.player_id }
    assert_redirected_to atribute_path(assigns(:atribute))
  end

  test "should destroy atribute" do
    assert_difference('Atribute.count', -1) do
      delete :destroy, id: @atribute
    end

    assert_redirected_to atributes_path
  end
end
